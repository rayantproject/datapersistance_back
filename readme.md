

# API pour le projet de persistance de données de l'application de gestion de gestion de commandes


cette API permet de gérer les commandes, les produits et les clientss.

## Installation

### Prérequis

- [deno](https://deno.land/)
- [mongoDB](https://www.mongodb.com/)
- [mongoDB compass](https://www.mongodb.com/products/compass)

### Installation

- Cloner le projet
- Installer les dépendances avec `deno task cache`

## Utilisation

### Lancement de l'API

- Lancer le serveur mongoDB
- ensuite executer la commande `deno task start` dans le dossier du projet pour lancer l'API


## Documentation

### Commandes

- `deno task start` : Lancer l'API
- `deno task test` : Lancer les tests
- `deno task dev` : Lancer l'API en mode développement (avec le module de reload)


## base de données


### Connexion

dans le fichier `.env` modifier la variable `DB_HOST` pour la mettre à jour avec l'URL de votre base de données mongoDB

- `mongodb://localhost:27017/` : si vous avez mongoDB en local sur le port 27017
- `mongodb://user:password@host:port/database` : si vous avez mongoDB en ligne sur un autre port ou avec un utilisateur et un mot de passe

### Structure

- `orders` : collection des commandes
- `users` : collection des utilisateurs

### Commandes

- `GET /orders` : Récupérer toutes les commandes
- `GET /orders/:id` : Récupérer une commande
- `POST /orders` : Créer une commande
- `PUT /orders/:id` : Modifier une commande
- `DELETE /orders/:id` : Supprimer une commande


### Users

- `GET /users` : Récupérer tous les utilisateurs
- `GET /users/:id` : Récupérer un utilisateur
- `POST /users` : Créer un utilisateur
- `PUT /users/:id` : Modifier un utilisateur
- `DELETE /users/:id` : Supprimer un utilisateur


## Développement

### Structure

- `src` : dossier contenant le code source de l'API
- `src/controllers` : dossier contenant les controllers de l'API
- `src/models` : dossier contenant les modèles de l'API
- `src/routes` : dossier contenant les routes de l'API
- `src/config` : dossier contenant les fichiers de configuration de l'API(tels que le fichier .de connexion à la base de données mongoDB)

### Dépendances

- [express](expressjs.com) : Framework pour créer des API
- [mongodb](https://www.npmjs.com/package/mongodb) : Driver pour se connecter à une base de données mongoDB
- [dotenv](https://www.npmjs.com/package/dotenv) : Module pour charger les variables d'environnement depuis un fichier .env


## Auteur

- [Enzo Anzite](https://github.com/EnzoAnzite)
- [Ibrahima Dia](https://github.com/ibrahimadia32)
- [Bamba Niang](https://github.com/ghost04k)
- [Rayan Traore](https://github.com/rayantProject)


