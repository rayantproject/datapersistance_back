import express, {Request,  Response, NextFunction } from "express";

import router from "./src/router/router.ts";
const port = 8000;

const app = express();


app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// app.use(cors());
app.use(
    (req: Request, res: Response, next: NextFunction) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
        next();
    }
)


app.use("/api", router)

app.use((req: Request, res: Response, next: NextFunction) => {
    res.status(404).json({message: "Not Found"});
    // next();
});

app.use((err: Error,  res: Response, next: NextFunction) => {
    res.status(500).json({message: err.message });
    // next();
})



app.listen(port, () => 
    {
        console.log("http://localhost:"+port);
    }
);