import * as mongoose from 'mongoose';
import { load } from "https://deno.land/std/dotenv/mod.ts";

const env = await load();

const dbURL = `mongodb://${env["DB_HOST"]}:27017/`

mongoose.connect(dbURL)
.then(() => {console.log("Connected to MongoDB")})
.catch((err) => {console.log("Error connecting to MongoDB host is ", dbURL, err)})


export default mongoose
