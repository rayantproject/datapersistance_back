import mongoose from "../config/db.ts";

const orderSchema = new mongoose.Schema({
    _id: { type: mongoose.Schema.Types.ObjectId, auto: true },
    products: [
        {
            name: { type: String, required: true },
            quantity: { type: Number, required: true },
            price: { type: Number, required: true },
        },
    ],
    date : { type: Date, default: Date.now },
    number : { type: String, required: true, unique: true },
});

const orderModel = mongoose.model("Order", orderSchema);

export default orderModel;
