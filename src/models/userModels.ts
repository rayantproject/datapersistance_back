import mongoose from "../config/db.ts";


const userSchema = new mongoose.Schema({
    _id: { type: mongoose.Schema.Types.ObjectId, auto: true },
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    createAt: { type: Date, default: Date.now, auto: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },

});



const userModel = mongoose.model("User", userSchema);


export default userModel;
