import  Router  from 'express';


import {getUser,  getUsers, deleteUser, updateUser, login, register } from '../controllers/userController.ts';


const router = Router()
.get('/', getUsers)
.get('/:id', getUser)
// .post('/   ', createUser)
.put('/:id', updateUser)
.delete('/:id', deleteUser)
.post('/login', login)
// .post('/signup', createUser)
.post('/register', register);

export default router;

