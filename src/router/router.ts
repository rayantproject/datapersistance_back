import Router from "express";
import orderRoute from "./orderRoute.ts";
import userRoute from "./userRoute.ts";

const router = Router()

.use("/orders", orderRoute)
.use("/users", userRoute);



export default router;