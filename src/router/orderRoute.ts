import Router from "express";
import { getOrder, getOrders, updateOrder, createOrder, deleteOrder } from "../controllers/orderController.ts";


const router = Router()
.get("/", getOrders)
.get("/:id", getOrder)
.post("/", createOrder)
.put("/:id", updateOrder)
.delete("/:id", deleteOrder);

export default router;
