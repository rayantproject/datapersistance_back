import userModel from "../models/userModels.ts";

import { Response, NextFunction, Request } from "express";


export const getUsers = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const users = await userModel.find();
        res.status(200).json(users);
    } catch (error) {
        next(error);
    }
}

export const getUser = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user = await userModel.findById(req.params.id);
        res.status(200).json(user );
    } catch (error) {
        next(error);
    }
}

export const createUser = async (req: Request, res: Response, next: NextFunction) => {

    const user = new userModel(
        {
            id: req.body.id,
            email: req.body.email,
            password: req.body.password,
            firstname: req.body.firstname,
            lastname: req.body.lastname,

        }
    );
    try {
        const newUser = await user.save();
        res.status(201).json(newUser);
        } catch (error) {   
            next(error);
        }   
}

export const updateUser = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const user = await userModel.findByIdAndUpdate(req.params.id, req.body, { new: true })
        res.status(200).json(user);
    }catch (err)
    {
        next(err);

    }   

}

export const deleteUser = async (req: Request, res: Response, next: NextFunction) => 
    {
        try{
            const user = await userModel.findByIdAndDelete(req.params.id);
            res.status(200).json(user);
        }catch (err)
        {
            next(err);
        }


    }

export const login = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const user = await userModel.find({email: req.body.email, password: req.body.password});
        if(user.length > 0){
            res.status(200).json( {
                id: user[0]._id
            } );
        }else{
            res.status(401).json({ message: "Invalid email or password" });
        }
    }catch (err)
    {
        next(err);
        
    }
}

export const register = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const user = await userModel.find({email: req.body.email});
        if(user.length > 0){
            res.status(409).json({ message: "Email already exists" });
        }else{
            const newUser = new userModel(
                {
                    email: req.body.email,
                    password: req.body.password,
                    firstname: req.body.firstname,
                    lastname: req.body.lastname,
                    // createAt: Date.now(),
                }
            );
            const user = await newUser.save();
            res.status(201).json({
                id: user._id
            });
        }

    }catch (err)
    {
        next(err);
        
    }
}