

import orderModel from "../models/orderModels.ts";
import { Response, NextFunction, Request } from "express";


export const getOrders = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const orders = await orderModel.find();
        res.status(200).json( orders);
    } catch (error) {
        next(error);
    }
}

export const getOrder = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const order = await orderModel.findById(req.params.id);
        res.status(200).json(order);
    } catch (error) {
        next(error);
    }
}

export const createOrder = async (req: Request, res: Response, next: NextFunction) => {
    try {
       
        const order = new orderModel(
            {
                products: req.body.products,
                date: req.body.date,
                number: req.body.number,
            }
        );
        const newOrder = await order.save();
        res.status(201).json( newOrder );

        } catch (error) {   
            next(error);
        }   
}

export const updateOrder = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const order = await orderModel.findByIdAndUpdate(req.params.id, {
            products: req.body.products,
            date: req.body.date,
            number: req.body.number, 
        }, { new: true })
        res.status(200).json( order );
    }catch (err)
    {
        next(err);
    }
}


export const deleteOrder = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const order = await orderModel.findByIdAndDelete(req.params.id);
        res.status(200).json( order );
    }catch (err)
    {
        next(err);
    }
}



